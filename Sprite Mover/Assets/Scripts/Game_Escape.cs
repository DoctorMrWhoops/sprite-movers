﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Escape : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //check if the escape key has been pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //quits the application
            Application.Quit();
        }
    }
}
