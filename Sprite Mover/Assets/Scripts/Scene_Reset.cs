﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene_Reset : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //When a certain key is pressed it will run the code
        if (Input.GetButtonDown("Submit"))
        {
            //Restarts the scene putting the player back where they started
            SceneManager.LoadScene("SampleScene");
        }
    }

}