﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shift_Move : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // This if statement allows the user to press left shift and the up arrow to move one unit up.
        if ((Input.GetKeyDown(KeyCode.UpArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.UpArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(0.0f, 1.0f, 0.0f);
        }
        // This if statement allows the user to press left shift and the down arrow to move one unit down.
        if ((Input.GetKeyDown(KeyCode.DownArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.DownArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(0.0f, -1.0f, 0.0f);
        }
        // This if statement allows the user to press the left shift and the left arrow to move one unit left.
        if ((Input.GetKeyDown(KeyCode.LeftArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.LeftArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(-1.0f, 0.0f, 0.0f);
        }
        // This if statement allows the user to press the left shift and the right arrow to move one unit right.
        if ((Input.GetKeyDown(KeyCode.RightArrow)) && (Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.RightArrow)) && (Input.GetKeyDown(KeyCode.LeftShift)))
        {
            transform.Translate(1.0f, 0.0f, 0.0f);
        }
    }
}
