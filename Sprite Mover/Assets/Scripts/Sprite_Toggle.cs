﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprite_Toggle : MonoBehaviour
{
    public bool pause = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //checks if the P key is being pressed
        if (Input.GetKeyDown("p"))
        {
            //if the game is not paused then it pauses
            if(pause)
            {
                //pauses
                Time.timeScale = 1;
            }
            // the other option
            else
            {
                // Unpauses the game
                Time.timeScale = 0;
            }
            //switches the bool between true and false
            pause = !pause;
        }
    } 
}
